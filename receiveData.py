from E22Serial.E22Serial import E22SerialClass
import time

E22 = E22SerialClass(serialPort="COM8", debug = 1, normalStart = 1)

print("Start Receive Example...")

while True :
    receivedData = E22.readSerialData()
    if(receivedData is not None):
        print("Data: ", receivedData)