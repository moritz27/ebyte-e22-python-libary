from E22Serial.E22Serial import E22SerialClass
import time

E22 = E22SerialClass(serialPort="COM8", normalStart = 1, debug=1)

time.sleep(1)

E22.setConfig() # sets all config parameters according the Config.json in the E22Serial directory
#E22.setModuleAddress(0x1234) # sets the module address to hex:0x1234 or dec:4660
#E22.setNetworkAddress(0x0) # sets the network address to 0x0
#E22.setChannel(18)    # sets the channel to a frequency of 868,125 MHz

config = E22.readConfig()
for i in range (3, len(config)):
    print("Address: ", i, hex(config[i]))

E22.setNormalMode() # always return to the normal operating mode after setting new config

