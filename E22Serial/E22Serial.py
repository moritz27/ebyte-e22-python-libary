import io, os
def is_raspberrypi():
    try:
        with io.open('/sys/firmware/devicetree/base/model', 'r') as m:
            if 'raspberry pi' in m.read().lower(): return True
    except Exception: pass
    return False

raspberry = is_raspberrypi()    

if raspberry:
    import RPi.GPIO as GPIO
    M0 = 22
    M1 = 27

from collections import OrderedDict
from pathlib import Path
import serial, time, sys, json
from enum import Enum

class modes(Enum):
            NORMAL = 0
            CONFIG = 1
            WOR = 2
            SLEEP = 3
        
class E22SerialClass:
    def __init__(self, serialPort="/dev/ttyS0", baudrate=9600, debug = 0, normalStart = 0):

        self.debug = debug

        if raspberry:
            GPIO.setmode(GPIO.BCM)
            GPIO.setwarnings(False)
            GPIO.setup(M0,GPIO.OUT)
            GPIO.setup(M1,GPIO.OUT)
        
        if normalStart == 0:
            self.setNormalMode()
        else:
            self.currentMode = modes.NORMAL
        
        self.pathToConfig = str(Path(__file__).parent) + "/Config.json"
        self.server_config = OrderedDict()
        self.file = open(self.pathToConfig,"r") 
        self.ConfigOptions = json.loads(self.file.read()) 
        self.file.close()
        
        self.ser = serial.Serial(serialPort, baudrate)
        self.ser.flushInput()

    def readSerialData(self):
        if self.ser.inWaiting() > 0 :
            time.sleep(0.1)
            r_buff = self.ser.read(self.ser.inWaiting())
            if r_buff != "" :
                return r_buff

    def readSerialDataACK(self):
        if self.ser.inWaiting() > 0 :
            time.sleep(0.1)
            r_buff = self.ser.read(self.ser.inWaiting())
            if r_buff != "" :
                self.sendSerialData(0xAA)
                return r_buff

    def readSerialDataRssiACK(self):
        if self.ser.inWaiting() > 0 :
            time.sleep(0.1)
            r_buff = self.ser.read(self.ser.inWaiting())
            if r_buff != "" :
                received = {"Data":"", "RSSI":""}
                received["Data"] = r_buff[0:-1]
                received["RSSI"] = int(r_buff[-1])
                self.sendSerialData(received["RSSI"])
                return received

    def readSerialDataWithRSSI(self):
        if self.ser.inWaiting() > 0 :
            time.sleep(0.1)
            r_buff = self.ser.read(self.ser.inWaiting())
            if r_buff != "" :
                received = {"Data":"", "RSSI":""}
                received["Data"] = r_buff[0:-1]
                received["RSSI"] = int(r_buff[-1])
                return received

    def readSerialJsonWithRSSI(self):
        if self.ser.inWaiting() > 0 :
            time.sleep(0.1)
            r_buff = self.ser.read(self.ser.inWaiting())
            if r_buff != "" :
                received = {"Data":"", "RSSI":""}
                if r_buff[0:-1] != b'' :
                    received["Data"] = json.loads(r_buff[0:-1])
                else:
                    received["Data"] = None
                received["RSSI"] = int(r_buff[-1])
                return received
                
    def sendSerialData(self, data):
        if self.debug:
            print("Sending Data: ", data)
        self.ser.write((str(data)).encode())
        time.sleep(0.1)

    def sendSerialDataACK(self, data, attempts):
        if self.debug:
            print("Sending Data: ", data)
        for i in range (0, attempts):
            if self.debug:
                print("Sending Data attempts Number: ", i+1)
            self.ser.write((str(data)).encode())
            time.sleep(2)
            ACK_msg = None
            for j in range (0,10):
                ACK_msg = self.readSerialData()
                time.sleep(0.2)
                if ACK_msg is not None:
                    break
            if ACK_msg is not None:
                print(ACK_msg[0:-1])
                if int(ACK_msg[0:-1]) == 0xAA:
                    return(i)
            time.sleep(1)
        if self.debug:
            print("Sending Data failed after ", attempts, " attempts")

    def sendSerialDataRssiACK(self, data, attempts):
        if self.debug:
            print("Sending Data: ", data)
        for i in range (0, attempts):
            if self.debug:
                print("Sending Data attempts Number: ", i+1)
            self.ser.write((str(data)).encode())
            time.sleep(2)
            ACK_msg = None
            for j in range (0,10):
                ACK_msg = self.readSerialDataWithRSSI()
                time.sleep(0.2)
                if ACK_msg is not None:
                    if self.debug:
                        print(int(ACK_msg["Data"]), ACK_msg["RSSI"])
                    return(i+1, ACK_msg)            
            time.sleep(1)   
        if self.debug:
            print("Sending Data failed after ", attempts, " attempts")       
        return(attempts,"Error")      
               

    def sendSerialJson(self, obj):
        data = json.dumps(obj)
        if self.debug:
            print("Sending Object: ", data)
        self.ser.write(data.encode()) 
        time.sleep(0.1)

    def readRegisters(self, startAddr, length):
        ByteArray = bytearray(3)
        ByteArray[0] = 0xC1
        ByteArray[1] = startAddr
        ByteArray[2] = length
        self.ser.write(ByteArray)
        if self.debug:
            for i in range (0, len(ByteArray)):
        	    print(ByteArray[i])
        while self.ser.inWaiting() == 0 :
            time.sleep(0.1)
        r_buff = self.ser.read(self.ser.inWaiting())
        return r_buff
                    
    def setRegister(self, startAddr, data):
        ByteArray = bytearray(4)
        ByteArray[0] = 0xC0
        ByteArray[1] = startAddr
        ByteArray[2] = 0x01
        ByteArray[3] = data
        if self.debug:
            for i in range (0, len(ByteArray)):
        	    print(ByteArray[i])
        self.ser.write(ByteArray)
        while self.ser.inWaiting() == 0 :
            time.sleep(0.1)
        r_buff = self.ser.read(self.ser.inWaiting())
        time.sleep(1)
        return r_buff
        
    def setMultiRegister(self, startAddr, dataList):
        length = len(dataList)
        ByteArray = bytearray(3 + length)
        ByteArray[0] = 0xC0 #Command for Set Register
        ByteArray[1] = startAddr
        ByteArray[2] = length
        
        for data in range (0, len(dataList)):
            ByteArray[data+3] = dataList[data]
        if self.debug:
            for i in range (0, len(ByteArray)):
        	    print(ByteArray[i])
        self.ser.write(ByteArray)
        while self.ser.inWaiting() == 0 :
            time.sleep(0.1)
        r_buff = self.ser.read(self.ser.inWaiting())
        time.sleep(1)
        return r_buff		
        
    def setModuleAddress (self, address):
        if self.currentMode != modes.CONFIG:
            self.setConfigMode()
        if self.debug:
            print(address >> 8, address & 255)
        addressArray = [(address >> 8), (address & 255)]
        if self.debug:
            print(addressArray)
        result = self.setMultiRegister(startAddr=0x0, dataList=addressArray)
        time.sleep(1)
        return result
        
    def setNetworkAddress(self, address):
        if self.currentMode != modes.CONFIG:
            self.setConfigMode()
        if self.debug:
            print(address)
        result = self.setRegister(startAddr=0x2, data=address)
        time.sleep(1)
        return result
        
    def setChannel(self, channel):
        if self.currentMode != modes.CONFIG:
            self.setConfigMode()
        if self.debug:
            print(channel)
        result = self.setRegister(startAddr=0x5, data=channel)
        time.sleep(1)
        return result

        
    def setConfig(self):
        if self.currentMode != modes.CONFIG:	
            self.setConfigMode()
        result=""
        for register in self.ConfigOptions:
            if self.debug:
                print("# ", register)
            registerValue = 0
            address = int(self.ConfigOptions[register]["Address"],16)			
            for attribute in self.ConfigOptions[register]["Options"]:
                if self.debug:
                    print("## ", attribute, ": ", self.ConfigOptions[register]["Options"][attribute]["Default"])
                defaultValue = self.ConfigOptions[register]["Options"][attribute]["Default"]
                bitOffset = self.ConfigOptions[register]["Options"][attribute]["BitOffset"]
                BinData = self.ConfigOptions[register]["Options"][attribute]["Values"][defaultValue] << bitOffset
                registerValue += BinData
            #print (registerValue)
            result = self.setRegister(startAddr=address, data = registerValue)
            time.sleep(0.5)
        return result

            
    def readConfig(self):
        if self.currentMode != modes.CONFIG:
            self.setConfigMode()
        actualConfig = self.readRegisters(startAddr=0x0, length=0x7)
        time.sleep(1)
        #actualModuleAddress = actualConfig[5] << 8 + actualConfig[4]
        #print(actualModuleAddress)
        time.sleep(0.5)
        return actualConfig

    def readAmbientNoise(self):
        ByteArray = bytearray(6)
        ByteArray[0] = 0xC0
        ByteArray[1] = 0xC1
        ByteArray[2] = 0xC2
        ByteArray[3] = 0xC3
        ByteArray[4] = 0x0
        ByteArray[5] = 0x2
        #for i in range (0, len(ByteArray)):
        #	print(ByteArray[i])
        #print(ByteArray)
        self.ser.write(ByteArray)
        while self.ser.inWaiting() == 0 :
            time.sleep(0.5)
        r_buff = self.ser.read(self.ser.inWaiting())
        #print(r_buff)
        #return int(r_buff[-1])
        RSSI = {"current":"", "lastReceive":""}
        RSSI["current"] = int(r_buff[-2])
        RSSI["lastReceive"] = int(r_buff[-1])
        return(RSSI)
  
    def setNormalMode(self):
        self.currentMode = modes.NORMAL
        if self.debug:
            print("Entering", self.currentMode.name, "Mode..")
        if raspberry:
            GPIO.output(M0,GPIO.LOW)
            GPIO.output(M1,GPIO.LOW)
            time.sleep(1)   
        else:
            input("Set M0:LOW and M1:LOW! Press Enter to continue...") 

    def setConfigMode(self):
        self.currentMode = modes.CONFIG
        if self.debug:
            print("Entering", self.currentMode.name, "Mode..")
        if raspberry:
            GPIO.output(M0,GPIO.LOW)
            GPIO.output(M1,GPIO.HIGH)
            time.sleep(1)
        else:
            input("Set M0:LOW and M1:HIGH! Press Enter to continue...") 

    def setWorMode(self):
        self.currentMode = modes.WOR
        if self.debug:
            print("Entering", self.currentMode.name, "Mode")
        if raspberry:
            GPIO.output(M0,GPIO.HIGH)
            GPIO.output(M1,GPIO.LOW)
            time.sleep(1)
        else:
            input("Set M0:HIGH and M1:LOW! Press Enter to continue...") 
   
    def setDeepSleepMode(self):
        self.currentMode = modes.SLEEP
        if self.debug:
            print("Entering", self.currentMode.name, "Mode")
        if raspberry:
            GPIO.output(M0,GPIO.HIGH)
            GPIO.output(M1,GPIO.HIGH)
            time.sleep(1)         
        else:
            input("Set M0:HIGH and M1:HIGH! Press Enter to continue...")    
			
		