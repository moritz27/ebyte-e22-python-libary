from E22Serial.E22Serial import E22SerialClass
import time

E22 = E22SerialClass(serialPort="COM3", debug = 1, normalStart = 1)
i=0
delay_temp = 0

print("Start Sending Example...")

while True :
    delay_temp += 1
    if delay_temp > 80000000:
        E22.sendSerialData(i)
        i+= 1
        delay_temp = 0

