from E22Serial.E22Serial import E22SerialClass
import time

E22 = E22SerialClass(serialPort="COM8", debug = 0, normalStart = 1)
i=0
delay_temp = 0

print("Start Sending ACK (with RSSI) Example...")

while True :
    delay_temp += 1
    if delay_temp > 80000000:
        #E22.sendSerialDataACK(i,10) #receive a ACK without usfull content back
        status = E22.sendSerialDataRssiACK(i, 10) #receive a ACK with the RSSI value of the reception
        if status[1] != "Error":
            print("Successful Transmission after", status[0], "Tries with RSSI on receiving end (ACK content):", int(status[1]["Data"]), " RSSI of ACK Message:", status[1]["RSSI"])
        else:
            print("Failed Transmission after",  status[0])
        i+= 1
        delay_temp = 0

