from E22Serial.E22Serial import E22SerialClass
import time

E22 = E22SerialClass(serialPort="COM8", debug = 1, normalStart = 1)

print("Start Receive ACK (with RSSI) Example...")

while True :
    #receivedData = E22.readSerialDataACK() # send back useless byte 0xAA as ACK message  
    receivedData = E22.readSerialDataRssiACK() #send back the RSSI value from the received message
    if(receivedData is not None):
        #print("Data: ", receivedData)
        print("Data:", receivedData["Data"], " RSSI:", receivedData["RSSI"])