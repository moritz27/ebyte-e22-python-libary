from E22Serial.E22Serial import E22SerialClass
import time

E22 = E22SerialClass(serialPort="COM3", debug = 1, normalStart = 1)

print("Start Receive With RSSI Example...")

while True :
    receivedData = E22.readSerialDataWithRSSI()
    if(receivedData is not None):
        print("Data:", int(receivedData["Data"]), " RSSI:", receivedData["RSSI"])